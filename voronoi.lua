--[[
    Voronoi Graph creation module of the luaFortune library.

    Documentation and License can be found here:
    https://bitbucket.org/Jmaa/luafortune
--]]

local voronoi = {
    version = 2.1
}

--------------------------------------------------------------------------------
-- Binary Heap

local heap_methods = {}
local heap_mt = {__index=heap_methods}

local function new_heap ()
    -- Parameters: nil
    -- Return: Heap

    return setmetatable({len = 0},heap_mt)
end

function heap_methods:empty()
    -- Parameters: nil
    -- Return: boolean

    return self.len == 0
end

function heap_methods:insert(point)
    -- Parameters: Point|Event
    -- Return: nil

    self.len = self.len + 1
    local new_record = self[self.len]
    local index = self.len
    while index > 1 do
        local parent_index = math.floor(index / 2)
        local parent_rec = self[parent_index]
        if point.x < parent_rec.x then
            self[index] = parent_rec
        else
            break
        end
        index = parent_index
    end
    self[index] = point
end

function heap_methods:pop()
    -- Parameters: nil
    -- Return: Point|Event

    local result = self[1]

    local last = self[self.len]
    local last_x = last.x

    self[self.len] = self[1]
    self.len = self.len - 1

    local parent_index = 1
    while parent_index * 2 <= self.len do
        local index = parent_index * 2
        if index+1 <= self.len and self[index+1].x < self[index].x then
            index = index + 1
        end
        local child_rec = self[index]
        if last_x < child_rec.x then
            break
        end
        self[parent_index] = child_rec
        parent_index = index
    end
    self[parent_index] = last
    return result
end

voronoi.priority_queue = new_heap

--------------------------------------------------------------------------------
-- Object creation methods

local function new_point (x, y)
    -- Parameters: number, number
    -- Return: Point

    return {x=x,y=y}
end

local function new_event (x_pos, point, arc)
    -- Parameters: number, number, number, Arc
    -- Return: Event

    return {x=x_pos,pnt=point,a=arc,valid=true}
end

local function new_arc (point, prev, next)
    -- Parameters: Point, Arc, Arc
    -- Return: Arc

    return {p=point,prev=prev,next=next,e=nil,s1=nil,s2=nil}
end

local function new_seg (first_point)
    -- Parameters: number, number
    -- Return: Segment

    return {p1=first_point,p2=nil,done=false}
end

voronoi.new_point = new_point

----------------------------------------------
-- Misc functions

local function dist (p1, p2)
    -- Parameters: number, number, number, number
    -- Return: number

    return math.sqrt(math.pow(p1.x-p2.x,2)+math.pow(p1.y-p2.y,2))
end

local function quadratic_formula (p1, p2, l)
    -- Parameters: Point, Point, number
    -- Return: number

    local z1 = 2*(p1.x-l)
    local z2 = 2*(p2.x-l)

    local a = 1/z1 - 1/z2;
    local b = -2*(p1.y/z1 - p2.y/z2);
    local c = (p1.y*p1.y + p1.x*p1.x - l*l)/z1 - (p2.y*p2.y + p2.x*p2.x - l*l)/z2;

    return (-b -math.sqrt(b*b - 4*a*c))/(2*a);
end

local function intersection (p1, p2, l)
    -- Parameters: Point, Point, number
    -- Return: number, number

    local p_x, p_y, res_y = p1.x, p1.y

    if p1.x == p2.x then
        res_y = (p1.y+p2.y)/2
    elseif p2.x == l then
        res_y = p2.y
    elseif p1.x == l then
        res_y = p1.y
        p_x, p_y = p2.x, p2.y
    else
        res_y = quadratic_formula(p1,p2,l)
    end

    return (p_x^2 + (p_y-res_y)^2 - l*l)/(2*p_x-2*l), res_y;
end

local function circle (a, b, c)
    -- Parameters: Point, Point, Point
    -- Return: boolean, number, Point

    if (b.x-a.x)*(c.y-a.y)-(c.x-a.x)*(b.y-a.y) > 0 then
        return false
    end

    local A, B, C, D = b.x - a.x, b.y - a.y, c.x - a.x, c.y - a.y
    local G = 2*(A*(c.y-b.y) - B*(c.x-b.x))

    if G==0 then
        return false
    end

    local E = A*(a.x+b.x) + B*(a.y+b.y)
    local F = C*(a.x+c.x) + D*(a.y+c.y)

    local circle_point = new_point((D*E-B*F)/G, (A*F-C*E)/G)
    return true, circle_point.x+dist(a,circle_point), circle_point
end

---------------------------------------------

local function intersect (p1, arc, return_point)
    -- Parameters: Point, Arc, [boolean]
    -- Return: boolean, [Point]

    if arc.p.x == p1.x then
        return false
    end

    local a, b
    local p1_x, p1_y = p1.x, p1.y

    if arc.prev then
        _, a = intersection(arc.prev.p, arc.p, p1_x)
    end
    if arc.next then
        _, b = intersection(arc.p, arc.next.p, p1_x)
    end

    if (not arc.prev or a <= p1_y) and (not arc.next or p1_y <= b) then
        local point = return_point and new_point((arc.p.x^2+(arc.p.y-p1_y)^2-p1_x^2)/(2*arc.p.x-2*p1_x), p1_y)
        return true, point
    end
    return false
end


local function check_circle_event (arc, min_x, points_and_events)
    -- Parameters: Arc, number, priority_queue<Point|Event>
    -- Return: nil

    if arc.e and arc.e.x ~= min_x then
        arc.e.valid = false
    end
    arc.e = nil

    if not arc.prev or not arc.next then
        return
    end

    local is_circle, event_x, circle_point = circle(arc.prev.p, arc.p, arc.next.p)
    if is_circle and event_x > min_x then
        arc.e = new_event(event_x,circle_point,arc)
        points_and_events:insert(arc.e)
    end
end

local function front_insert (point, arc, output, points_and_events)
    -- Parameters: Point, Arc, array<Edge>, priority_queue<Point|Event>
    -- Return: nil

    while arc do
        local does_intercept, intersect_point = intersect(point, arc, true)
        if does_intercept then
            local does_intercept_1 = arc.next and intersect(point,arc.next, false)
            if arc.next and not does_intercept_1 then
                local arc_new = new_arc(arc.p,arc,arc.next)
                arc.next.prev = arc_new
                arc.next = arc_new
            else
                arc.next = new_arc(arc.p,arc)
            end
            arc.next.s2 = arc.s2

            arc.next.prev = new_arc(point,arc,arc.next)
            arc.next = arc.next.prev

            arc = arc.next

            arc.s1 = new_seg(intersect_point)
            arc.prev.s2 = arc.s1
            table.insert(output,arc.s1)

            arc.s2 = new_seg(intersect_point)
            arc.next.s1 = arc.s2
            table.insert(output,arc.s2)

            check_circle_event(arc,      point.x, points_and_events)
            check_circle_event(arc.prev, point.x, points_and_events)
            check_circle_event(arc.next, point.x, points_and_events)
            return
        else
            arc = arc.next
        end
    end
end

local function process_event (event, output, points_and_events)
    -- Parameters: Event, array<Edge>, priority_queue<Point|Event>
    -- Return: nil

    local arc = event.a

    local s1 = arc.s1
    local s2 = arc.s2
    if s1 and not s1.done then
        s1.p2, s1.done = event.pnt, true
    end
    if s2 and not s2.done then
        s2.p2, s2.done = event.pnt, true
    end

    local segment = new_seg(event.pnt)
    table.insert(output,segment)

    if arc.next then
        arc.next.prev = arc.prev
        arc.next.s1 = segment
    end
    if arc.prev then
        arc.prev.next = arc.next
        arc.prev.s2 = segment
        check_circle_event(arc.prev,event.x,points_and_events)
    end
    if arc.next then
        check_circle_event(arc.next,event.x,points_and_events)
    end
end


local function finish_edges (arc, p1_x, p1_y, p2_x, p2_y)
    -- Parameters: Arc, number, number, number, number
    -- Return: nil

    local l = p2_x + (p2_x-p1_x)+(p2_y-p1_y)
    while arc do
        local s2 = arc.s2
        if s2 and not s2.done then
            s2.done, s2.p2 = true, new_point(intersection(arc.p, arc.next.p, l*2))
        end
        arc = arc.next
    end
end

--------------------------------------------------------------------------------


local function liangBarsky (p1_x, p1_y, p2_x, p2_y ,borderLeft, borderRight, borderBottom, borderTop)
    local t1, t2 = 0, 1
    local xdelta, ydelta = p2_x-p1_x, p2_y-p1_y

    for border = 1, 4 do
        local p, q
        if border == 1 then
            p, q = -xdelta, -(borderLeft-p1_x)
        end
        if border == 2 then
            p, q =  xdelta,  (borderRight-p1_x)
        end
        if border == 3 then
            p, q = -ydelta, -(borderBottom-p1_y)
        end
        if border == 4 then
            p, q =  ydelta,  (borderTop-p1_y)
        end

        if p==0 and q < 0 then
            return nil
        end

        local r = q/p
        if p < 0 then
            if r > t2 then
                return nil
            elseif r > t1 then
                t1 = r
            end
        elseif p > 0 then
            if r < t1 then
                return nil
            elseif r < t2 then
                t2 = r
            end
        end
    end

    return p1_x + t1*xdelta, p1_y + t1*ydelta, p1_x + t2*xdelta, p1_y + t2*ydelta
end



local function cut_edges (edges, border_left, border_right, border_top, border_bottom)
    local atan2 = math.atan2

    local boundary_points = {}
    local center_x, center_y = (border_left+border_right)/2, (border_bottom+border_top)/2
    for i=#edges, 1, -1 do
        local edge = edges[i]
        local p1, p2 = edge.p1, edge.p2
        local n1_x, n1_y, n2_x, n2_y = liangBarsky(p1.x, p1.y, p2.x, p2.y, border_left, border_right, border_bottom, border_top)
        if not n1_x then
            table.remove(edges,i)
        elseif p1.x ~= n1_x and p1.y ~= n1_y then
            edge.p1 = new_point(n1_x,n1_y)
            edge.p1.r = atan2(n1_x-center_x,n1_y-center_y)
            table.insert(boundary_points,edge.p1)
        elseif p2.x ~= n2_x and p2.y ~= n2_y then
            edge.p2 = new_point(n2_x,n2_y)
            edge.p2.r = atan2(n2_x-center_x,n2_y-center_y)
            table.insert(boundary_points,edge.p2)
        end
    end

    local corner_angles = {
        math.pi,
        math.atan2(border_right-center_x,border_bottom-center_y),   -- Top Right
        math.atan2(border_right-center_x,border_top-center_y),      -- Bottom Right
        math.atan2(border_left-center_x,border_top-center_y),       -- Bottom Left
        math.atan2(border_left-center_x,border_bottom-center_y),    -- Top Left
    }
    local next_angel = table.remove(corner_angles)

    table.sort(boundary_points,function(a,b) return a.r < b.r end)
    local min, max = math.min, math.max
    for i=1, #boundary_points do
        local point_1, point_2 = boundary_points[i], boundary_points[i%#boundary_points+1]
        if next_angel < point_2.r then
            local corner_id = #corner_angles
            local new_point = new_point(
                corner_id<3 and max(point_1.x,point_2.x) or min(point_1.x,point_2.x),
                (corner_id==2 or corner_id==3) and max(point_1.y,point_2.y) or min(point_1.y,point_2.y)
            )
            table.insert(edges,{p1=point_1,p2=new_point})
            table.insert(edges,{p1=point_2,p2=new_point})
            next_angel = table.remove(corner_angles) or 5
        else
            table.insert(edges,{p1=point_1,p2=point_2})
        end
    end
end

voronoi.fortunes_algorithm = function (start_points, p1_x, p1_y, p2_x, p2_y)
    -- Parameters: array<Point>, number, number, number, number
    -- Return: array<Edge>

    local points_and_events = voronoi.priority_queue()
    local output = {}

    for _, point in ipairs(start_points) do
        points_and_events:insert(point)
    end

    local root_point = points_and_events:pop()
    local root = new_arc(root_point)

    while not points_and_events:empty() do
        local point_or_event = points_and_events:pop()
        if point_or_event.valid == nil then -- It's a point
            front_insert(point_or_event,root,output, points_and_events)
        elseif point_or_event.valid then -- It's an event
            process_event(point_or_event,output, points_and_events)
        end
    end

    finish_edges(root, p1_x, p1_y, p2_x, p2_y)

    cut_edges(output, p1_x, p2_x, p2_y, p1_y)

    return output
end

--------------------------------------------------------------------------------

local function get_points_from_point (point, came_from, point_relations)
    local p1, p2, p3 = point_relations[point][1], point_relations[point][2], point_relations[point][3]
    if not p2 then
        return p1
    elseif came_from == p1 then
        p1, p2 = p2, p3
    elseif came_from == p2 then
        p2 = p3
    end

    if not p2 or (point.x-came_from.x)*(p1.y-came_from.y) - (point.y-came_from.y)*(p1.x-came_from.x) > 0 then
        return p1
    else
        return p2
    end
end

local function remove_point_from_point (point, other_point, points)
    for i, dir_point in ipairs(points[point]) do
        if dir_point == other_point then
            table.remove(points[point],i)
            break
        end
    end
    if #points[point] == 0 then
        points[point] = nil
    end
end

local function select_random_points (points)
    for point, relations in pairs(points) do
        return point, relations[1]
    end
end

voronoi.find_faces_from_edges = function (edges)
    --local edge_stuff = {}
    local points = {}
    for _, edge in ipairs(edges) do
        if not points[edge.p1] then
            points[edge.p1] = {}
        end
        table.insert(points[edge.p1],edge.p2)
        if not points[edge.p2] then
            points[edge.p2] = {}
        end
        table.insert(points[edge.p2],edge.p1)
    end

    local faces = {}
    local path = {edges[1].p1,edges[1].p2}

    while true do
        remove_point_from_point(path[#path-1],path[#path],points)
        if path[1] ~= path[#path] then
            local next_point = get_points_from_point(path[#path],path[#path-1],points)
            table.insert(path, next_point)
        else
            table.remove(path)
            table.insert(faces, path)
            path = {select_random_points(points)}
            if not path[1] then
                break
            end
        end
    end
    return faces
end

---------------------

return voronoi
