# luaFortune ###################################################################

## Introduction ################################################################

A library for generating various tessellations and texturing of a plane, in Lua.
In less formal speech: Tools for splitting up a plane into smaller parts, in
Lua.

The eventual goal of the library is to become a goto, for resources pertaining
to the creation of procedural content generation algorithms.

It also have an extremely dismissive license, so feel free to look at and copy
the code. I'm simply interested in spreading my knowledge about these subjects.

The library is split into smaller modules, with some of them sharing code
though the `base` module. You can mix-and-max depending on your want and needs.

Currently the library contains:

-   **Voronoi**: By using Fortune's Algorithm, it is possible to quickly and
    easily generate a voronoi tessellation of a space with any set of points.
    See the `Voronoi` module.
-   **Binary Space Partioning**: A technique usually used for storing points, it
    can be used for dividing a space into interestingly formed areas.
    See the `bsp` module.
-   **Random Points**: Module for generating a large number of points, with
    different noise frequencies. Also generating within an arbitrary polygon.

Future Expansion:

-   **Tiling**: Sometimes even basic shapes are useful, and this module will
    help with that.
-   **(Fractal) Textures**: Through the use of Perlin, Simplex or OpenSimplex
    this module will allow the easy creation of fractal textures.
-   **Delaunay**: The opposite of Voronoi, this tessellation is just as useful.
    Expect an implementation of Ruppert's Algorithm.

## Overview ####################################################################

Below is the documentation for the project in general, including how to install.
If you'd rather look at the docs for a specific module:

-   [**Voronoi**](docs/voronoi.md)
-   [**Binary Space Partioning**](docs/bsp.md)
-   [**Random Points**](docs/random_points.md)

## Examples ####################################################################

Ok, I understand you. I've said a lot of fancy words, but haven't shown you
anything to win you over. Well then. I too can play that game.

The following was implemented with [LÖVE](https://love2d.org/).

![Animated voronoi graph, based on random particles using the *awesome* LÖVE
engine](http://i.imgur.com/22LQAnO.gif)

![Dividing a map into smaller and smaller
irregular pieces](http://i.imgur.com/FAOwSuS.gif)

## How to Use ##################################################################

### Dependencies & Installation ################################################

The library has been tested with `PUC Lua 5.1` and `LuaJIT 2.0.3`. Other
versions of Lua may or may not work. I'd appreciate users of other Lua versions
to test and report their finding to me.

No external libraries required. The modules are plug-and-play. No internal
dependencies either.

For installation, simply drop the module you want into your directory of choice,
and it should work nicely.

### Tests ######################################################################

Test for the modules can be found in the `test` directory.

For reasons I cannot discern tests must be run in either the `test` directory
or in the `test/[module]` directory.

The `main.lua` file in each test directory, is the main file for a LÖVE project.
Running the folder with love will show some nice visual tests of the module.

## Contributing ################################################################

If you have ideas for features or faster algorithms or know how to code, please
feel free to share. I'd love to expand the library with more features useful for
map makers.

There are just there few points to consider before you contribute:

-   **The Maintainer**: I survey everything going in. Don't worry, the bar isn't
    high, and I'll come with considerable response, in case anything doesn't
    make it.
-   **The Code**: I'm not picky when it comes to code, but please try to
    maintain the theme, and general interface.
-   **The License**: Please be aware that anything you submit becomes covered
    under the license. This may not be what you intended, think about it first.

## License #####################################################################

As I said in the introduction, the license is extremely permissive. This project
is really more about sharing what I've learned, than restricting my knowledge
behind some stupid barriers.

Feel free to do anything with the code, including learning, sharing and using
it in any project, be they commercial or not.

### THE BEER-WARE LICENSE ######################################################

```text
<jonjmaa@gmail.com> wrote this library.  As long as you include a namedrop
you can do whatever you want with this stuff. If we meet some day, and you
think this stuff is worth it, you can buy me a beer in return.
- Jon Michael Aanes
```

# Credits ######################################################################

Library made by:

-   Jon Michael Aanes (2015)

Thanks go to (Voronoi):

-   Steve J. Fortune (1986) for his paper "A sweepline algorithm for Voronoi
    diagrams.", which described the algorithm named after him: "Fortune's
    Algorithm", and for publishing the original C code.
-   Matt Brubeck (2002) for
    [his C++ port](https://www.cs.hmc.edu/~mbrubeck/voronoi.html), which
    this implementation of Fortune's Algorithm is heavily based on.
