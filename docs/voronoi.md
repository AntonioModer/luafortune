# Voronoi ######################################################################

## Introduction ################################################################

A simple and fast Lua port of Fortune's Algorithm.

Useful for map generation, tracing cholora outbreaks to their origin, [and a
lot more](https://en.wikipedia.org/wiki/Voronoi_diagram#Applications). For
most people looking here, I'll take the wager and say it'll be map
generation.

![Animated voronoi graph, based on random particles using the *awesome* LÖVE
engine](http://i.imgur.com/22LQAnO.gif)

## How To Use ##################################################################

### Dependencies & Installation ################################################

Plug-and-play, no internal or external dependencies.

### Quick Start ################################################################

The module has defines two primary functions:

-   `fortunes_algorithm`: Accepts a list of points, and a boundary box, and
    returns a list of edges, defined as the voronoi diagram for the input
    points.
-   `find_faces_from_edges`: Accepts a list of points, like that produced by
    `fortunes_algorithm` and returns a list of polygons (or faces, if you
    want the proper term.)

The idea is that if you only want the edges/borders, you don't need to use
`find_faces_from_edges` at all.

Example:

```lua
local voronoi = require "voronoi"
local points = {}
for i=1, 10 do
    points[i] = voronoi.new_point(math.random(),math.random())
    --Or: points[i] = {x=math.random(),y=math.random()}
end
local edges = voronoi.fortunes_algorithm(points,0,0,1,1)
print("Edges: ")
for i, edge in ipairs(edges) do
    print("-",edge.p1.x,edge.p1.y,edge.p2.x,edge.p2.y)
end
local faces = voronoi.find_faces_from_edges(edges)
print("Polygons/Faces: ")
for i, face in ipairs(faces) do
    print("- Face nr. "..i)
    for j, point in ipairs(face) do
        print("\t *",point.x,point.y)
    end
end
```

### "Full Documentation" #######################################################

Exposed functions:

-   `voronoi.new_point(number, number)`: Returns a `Point`.
-   `voronoi.fortunes_algorithm(list<Point>, number, number, number, number)`:
    Returns a list of voronoi graph edges.
-   `voronoi.find_faces_from_edges(list<Edge>)`: Returns a list of
    polygons (which are lists of points).
-   `voronoi.priority_queue()`: Function returns a new priority queue
    with the `:insert(point_or_event)`, `:pop()` and `:empty()` methods.
    (You can overwrite this if you want.)

Exposed variables:

-   `voronoi.version`: The current version of the module.

## License & Credits ###########################################################

The license can be found in `LICENSE.txt`, or along with the credits in [in the
top level documentation](../README.md).
